//
//  ViewModelTests.swift
//  Challenge Part 2Tests
//
//  Created by The App Experts on 04/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
@testable import Challenge_Part_2

class ViewModelTests: XCTestCase {

    var sut: ViewModel!
    var testConfig: URLSessionConfiguration!
    override func setUp() {
        super.setUp()
        let networkManager = NetworkManager()
        sut = ViewModel(networkManager: networkManager)
        
        let urlString = "https://my-json-server.typicode.com/shanev96/challenge/db"
        
        // create and attach fixed data in our protocol handler
        let account = AccountModel()
        var root = RootObject()
        // add 3 accounts to test data
        for _ in 1...3 {
            root.accounts.append(account)
        }
        // encode data to JSON data so it matches the expected type
        let data = try! JSONEncoder().encode(root)
        URLProtocolMock.testURLs = [URL(string: urlString): data]
        
        // now set up a configuration to use our mock
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        testConfig = config
    }

    override func tearDown() {
        testConfig = nil
        sut = nil
        super.tearDown()
        
    }
    
    func testGetAccountDataFromNetworkManager() {
        let ex = expectation(description: "accounts loaded")
        // load data with custom config
        sut.getAccountDataFromNetworkManager(configuration: testConfig)
        
        sut.bindViewModelToController = {
            // check model contains data
            XCTAssertNotEqual(self.sut.numberOfRowsInSection(), 0)
            ex.fulfill()
        }
        waitForExpectations(timeout: 5)
        
    }
    
    func testNumberOfSectionsWithoutData() {
        XCTAssertEqual(sut.numberOfSections, 0)
    }
    
    func testNumberOfSectionsWithData() {
        let ex = expectation(description: "accounts loaded")
        // load data with test config
        sut.getAccountDataFromNetworkManager(configuration: testConfig)
        sut.bindViewModelToController = {
            // check model contains data
            XCTAssertEqual(self.sut.numberOfSections, 1)
            ex.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
    
    func testNumberOfRowsInSection() {
        let ex = expectation(description: "accounts loaded")
        // load data with test config
        sut.getAccountDataFromNetworkManager(configuration: testConfig)
        sut.bindViewModelToController = {
            // check model contains data
            XCTAssertEqual(self.sut.numberOfRowsInSection(), 3)
            ex.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
    
    
    func testItemAt() {
        let ex = expectation(description: "Got item")
        // load data with test config
        sut.getAccountDataFromNetworkManager(configuration: testConfig)
        sut.bindViewModelToController = {
            let indexPath = IndexPath(item: 1, section: 1)
            // get item from model
            let account = self.sut.item(at: indexPath)
            // check item contains data
            XCTAssertNotNil(account)
            ex.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
    
    func testItemAtBoundary() {
        let ex = expectation(description: "Got item")
        // load data with test config
        sut.getAccountDataFromNetworkManager(configuration: testConfig)
        sut.bindViewModelToController = {
            let indexPath = IndexPath(item: 0, section: 1)
            // get item from model
            let account = self.sut.item(at: indexPath)
            // check item contains data
            XCTAssertNotNil(account)
            ex.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
    
    func testItemAtInvalidSection() {
        let ex = expectation(description: "Invalid section")
        // load data with test config
        sut.getAccountDataFromNetworkManager(configuration: testConfig)
        sut.bindViewModelToController = {
            let indexPath = IndexPath(item: 0, section: 5)
            // attempt to get item at invalid path
            let account = self.sut.item(at: indexPath)
            // check item is nil
            XCTAssertNil(account)
            ex.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
    
    func testItemAtInvalidRow() {
        let ex = expectation(description: "Invalid item")
        // load data with test config
        sut.getAccountDataFromNetworkManager(configuration: testConfig)
        sut.bindViewModelToController = {
            let indexPath = IndexPath(item: -1, section: 0)
            // attempt to get item at invalid path
            let account = self.sut.item(at: indexPath)
            // check item is nil
            XCTAssertNil(account)
            ex.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
    
}
