//
//  ViewModel.swift
//  Challenge Part 2
//
//  Created by The App Experts on 04/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ViewModel {
    
    var networkManager: NetworkManager

    private(set) var accounts: [AccountModel]! {
        didSet {
            // perform closure after data has been set
            self.bindViewModelToController()
        }
    }
    
    // closure to perform updates once data has been set
    var bindViewModelToController : (() -> ())
    
    init(networkManager: NetworkManager) {
        self.bindViewModelToController = {}
        self.networkManager = networkManager
        self.accounts = []
    }
    
    func getAccountDataFromNetworkManager(configuration: URLSessionConfiguration = .default) {
//        networkManager.getAccountDataFromFile() { (accounts) in
//            self.accounts = accounts
//        }
        
        networkManager.setAccountDataFromServer(configuration: configuration) { (accounts) in
            // set account data to model
            self.accounts = accounts
        }
    }
}

extension ViewModel {
    var numberOfSections: Int {
        if accounts.isEmpty || accounts == nil {
            return 0
        }
        return 1 //return sections
    }
    
    func numberOfRowsInSection() -> Int {
        return accounts.count //use section too if needed
    }
    
    func item(at indexPath: IndexPath) -> AccountModel? {
        if indexPath.section < 0 || indexPath.section > numberOfSections {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection() {
            return nil
        }
        
        return accounts[indexPath.row] // return item from model
    }
}
