//
//  NetworkManager.swift
//  Challenge Part 2
//
//  Created by The App Experts on 04/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

class NetworkManager {
    
    var accounts: [AccountModel]
    
    init() {
        self.accounts = []
    }
    
    func setAccountDataFromServer(configuration: URLSessionConfiguration = .default, completion: @escaping (([AccountModel]) -> ())) {
        // set url
        let urlString = "https://my-json-server.typicode.com/shanev96/challenge/db"
        // load data from api
        loadJsonFrom(urlString: urlString,  configuration: configuration) { (result) in
            switch (result) {
            case .success(let data):
                // parse data if successful
                self.parseJsonFromServer(jsonData: data)
                //display data
                completion(self.accounts)
            case .failure(let error):
                // handle error
                print(error)
            }
        }
    }
    
    // load JSON
    func loadJsonFrom(urlString: String, configuration: URLSessionConfiguration = .default, completion: @escaping (Result<Data, Error>) -> Void) {
        // check url is valid
        if let url = URL(string: urlString) {
            // create url session
            let urlSession = URLSession(configuration: configuration).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    // handle error
                    completion(.failure(error))
                }
                
                if let data = data {
                    // handle data
                    completion(.success(data))
                }
            }
            urlSession.resume()
        }
    }
    
    private func parseJsonFromServer(jsonData: Data) {
        do {
            // decode JSON data using model
            let decodedData = try JSONDecoder().decode(RootObject.self,
                                                       from: jsonData)
            // retrieve accounts
            self.accounts = decodedData.accounts
        } catch {
            // handle error
            print("decode error")
        }
    }
    
}
