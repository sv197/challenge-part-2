//
//  DependancyFactory.swift
//  Challenge Part 2
//
//  Created by The App Experts on 04/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

struct DependencyFactory {

    func createNetworkManager() -> NetworkManager {
        // create and return network manager
        return NetworkManager()
    }
    
    func createViewModel() -> ViewModel {
        // create network manager for view model
        let networkManager = createNetworkManager()
        // create view model and pass network manager
        let viewModel = ViewModel(networkManager: networkManager)
        // return view model
        return viewModel
    }
    
    func createDetailViewModel(selectedAccount: AccountModel) -> DetailViewModel {
        return DetailViewModel(account: selectedAccount)
    }
}
