//
//  AccountModel.swift
//  Challenge Part 2
//
//  Created by The App Experts on 04/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

struct RootObject: Codable {
    var accounts: [AccountModel] = []
}

struct AccountModel: Codable {
    var kind: String = ""
    var title: String = ""
    var number: String = ""
    var balance: Double = 0
    var currency: String = ""
    var transaction: [Transaction] = []
}

struct Transaction: Codable {
    var name: String = ""
    var type: String = ""
    var date: String = ""
    var status: String = ""
    var amount: Double = 0
}
