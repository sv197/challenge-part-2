//
//  DetailViewController.swift
//  Challenge Part 2
//
//  Created by The App Experts on 16/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var accountTypeLabel: UILabel!
    @IBOutlet var accountNumberLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    var viewModel: DetailViewModel!
    

    override func viewDidLoad() {
            super.viewDidLoad()
            self.displayDetails()
            self.updateTable()
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 160
        }

    func updateTable(){
        DispatchQueue.main.async {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    func displayDetails(){
        guard let account = viewModel.selectedAccount else {
            return
        }
        self.title = account.title
        self.accountTypeLabel?.text? += account.kind
        self.accountNumberLabel?.text? += account.number
        self.balanceLabel?.text? += account.currency + String(account.balance)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TransactionCell else {
            return tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        }
        guard let transaction = viewModel.item(at: indexPath) else {
            return cell
        }
        cell.nameLabel.text? = "Name: \(transaction.name)"
        cell.amountLabel.text? += "Amount: \(transaction.amount)"
        cell.dateLabel.text? += "Date: \(transaction.date)"
        cell.statusLabel.text? += "Status: \(transaction.status)"
        cell.typeLabel.text? += "Type: \(transaction.type)"
        cell.sizeToFit()
        return cell
    }
    
    
}
