//
//  DetailViewModelTests.swift
//  Challenge Part 2Tests
//
//  Created by The App Experts on 16/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
@testable import Challenge_Part_2

class DetailViewModelTests: XCTestCase {
    
    var sut: DetailViewModel!
    
    override func setUp() {
        super.setUp()
        // create test account
        var account = AccountModel()
        let transaction = Transaction()
        account.transaction = [transaction]
        // initialise view model
        sut = DetailViewModel(account: account)
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testItemAt() {
        let indexPath = IndexPath(item: 0, section: 1)
        // get item from model
        let transaction = sut.item(at: indexPath)
        // check item contains data
        XCTAssertNotNil(transaction)
    }
    
    func testItemAtInvalidSection() {
        let indexPath = IndexPath(item: 0, section: 5)
        // attempt to get item at invalid path
        let transaction = self.sut.item(at: indexPath)
        // check item is nil
        XCTAssertNil(transaction)
    }
    
    func testItemAtInvalidRow() {
        let indexPath = IndexPath(item: -1, section: 0)
        // attempt to get item at invalid path
        let transaction = self.sut.item(at: indexPath)
        // check item is nil
        XCTAssertNil(transaction)
    }
    
    func testNumberOfSections() {
        XCTAssertEqual(sut.numberOfSections, 1)
    }
    
    func testNumberOfRowsInSection() {
        XCTAssertEqual(sut.numberOfRowsInSection(), 1)
    }
    
}
