//
//  AccountsTableViewController.swift
//  Challenge Part 2
//
//  Created by The App Experts on 04/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class AccountsTableViewController: UITableViewController {

    var viewModel: ViewModel = DependencyFactory().createViewModel()
    var selectedAccount: AccountModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // load data for table
        getDataFromViewModelForUIUpdate()
        tableView.tableFooterView = UIView()
    }

    func getDataFromViewModelForUIUpdate() {
        // initiate data retrieval from API
        viewModel.getAccountDataFromNetworkManager()
        self.viewModel.bindViewModelToController = {
            // update table once data has been retrieved from server
            self.updateTable()
        }
    }
    
    // update table view
    private func updateTable() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // retrieve from view model
        return viewModel.numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // retrieve from view model
        return viewModel.numberOfRowsInSection()
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // dequeue cell from table
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        // get item from the view model
        guard let account = viewModel.item(at: indexPath) else {
            // return empty cell if no item exists
            return cell
        }
        // display account title
        cell.textLabel?.text = account.title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selected = viewModel.item(at: indexPath) {
            selectedAccount = selected
            performSegue(withIdentifier: "showDetails", sender: self)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "showDetails") {
            guard let vc = segue.destination as? DetailViewController else {
                return
            }
            vc.viewModel = DependencyFactory().createDetailViewModel(selectedAccount: selectedAccount)
            
        }
    }
    

}
