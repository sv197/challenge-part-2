//
//  Challenge_Part_2Tests.swift
//  Challenge Part 2Tests
//
//  Created by The App Experts on 04/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
@testable import Challenge_Part_2

class NetworkManagerTests: XCTestCase {
    
    var sut: NetworkManager!
    
    override func setUp() {
        super.setUp()
        sut = NetworkManager()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testLoadJsonFrom() {
        // this is the URL we expect to call
        let urlString = "https://my-json-server.typicode.com/shanev96/challenge/db"
        
        // create and attach fixed data in our protocol handler
        let account = AccountModel()
        var root = RootObject()
        root.accounts.append(account)
        // encode data to JSON data so it matches the expected type
        let data = try! JSONEncoder().encode(root)
        URLProtocolMock.testURLs = [URL(string: urlString): data]
        
        // now set up a configuration to use our mock
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        
        let ex = expectation(description: "data retrieved")
        // load data with custom config
        sut.loadJsonFrom(urlString: urlString, configuration: config) { (result) in
            switch (result) {
            case .success( _):
                ex.fulfill()
            case .failure(let error):
                print(error)
            }
        }
        waitForExpectations(timeout: 5)
    }
    
    func testFailLoadJsonFrom() {
        // create invalid url
        let urlString = "invalidURLString"
        
        let ex = expectation(description: "error")
        // try to load data
        sut.loadJsonFrom(urlString: urlString) { (result) in
            switch (result) {
            case .success( _):
                print("")
            case .failure( _):
                ex.fulfill()
            }
        }
        waitForExpectations(timeout: 5)
    }
    
}
