//
//  DependencyFactoryTests.swift
//  Challenge Part 2Tests
//
//  Created by The App Experts on 05/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
@testable import Challenge_Part_2

class DependencyFactoryTests: XCTestCase {

    var sut: DependencyFactory!
    
    override func setUp() {
        super.setUp()
        sut = DependencyFactory()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testCreateNetworkManager() {
        // create network manager using factory
        let testNetworkManager = sut.createNetworkManager()
        // manually create network manager
        let networkManager = NetworkManager()
        // compare
        XCTAssertEqual(String(describing: testNetworkManager.self), String(describing: networkManager.self))
    }
    
    func testCreateViewModel() {
        // create view model using factory
        let testViewModel = sut.createViewModel()
        // manually create view model
        let networkManager = NetworkManager()
        let viewModel = ViewModel(networkManager: networkManager)
        // compare
        XCTAssertEqual(String(describing: testViewModel.self), String(describing: viewModel.self))
    }
    
    func testCreateDetailViewModel() {
        let account = AccountModel()
        let testDetailViewModel = sut.createDetailViewModel(selectedAccount: account)
        let detailViewModel = DetailViewModel(account: account)
        XCTAssertEqual(String(describing: testDetailViewModel.self), String(describing: detailViewModel.self))
    }
    
}
