//
//  DetailViewModel.swift
//  Challenge Part 2
//
//  Created by The App Experts on 16/11/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

class DetailViewModel {

    private(set) var selectedAccount : AccountModel! {
        didSet {
            self.bindViewModelToController()
        }
    }
    
    var bindViewModelToController : (() -> ()) = {}

    init(account: AccountModel) {
        setAccountData(account: account)
    }
    
    func setAccountData(account: AccountModel) {
        selectedAccount = account
    }

}

extension DetailViewModel {
    var numberOfSections: Int {
        return 1 //return sections
    }
    
    func numberOfRowsInSection() -> Int {
        return selectedAccount.transaction.count //use section too if needed
    }
    
    func item(at indexPath: IndexPath) -> Transaction? {
        if indexPath.section < 0 || indexPath.section > numberOfSections {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection() {
            return nil
        }
        
        return selectedAccount.transaction[indexPath.row]
    }
}
